const express = require('express')
const digiTransitApiProxy = require('./routes/digiTransitApiProxy')
const middlewares = require('./middlewares')

const app = express()

app.use('/', digiTransitApiProxy)

app.use(middlewares.logErrors)
app.use(middlewares.errorHandler)

module.exports = app