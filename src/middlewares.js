const errorHandler = (err, req, res, next) => {
  const statusCode = res.statusCode !== 200 ? res.statusCode : 500
  res.status(statusCode)
    .json({
      message: err.message,
      response: err.response.data,
      stack: process.env.NODE_ENV === 'production' ? '🥞' : err.stack
    })
}

const logErrors = (err, req, res, next) => {
  console.error(err.stack)
  next(err)
}

module.exports = {
  errorHandler,
  logErrors
}
