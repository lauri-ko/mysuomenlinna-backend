const router = require('express').Router()
const bodyParser = require('body-parser')
const axios = require('axios')

const BASE_URL = 'http://api.digitransit.fi/routing/v1/routers/hsl/index/graphql'
const config = {
  headers: {
    'Content-Type': 'application/json',
  }
}

router.use(bodyParser.json())

router.post('/', (req, res, next) => {
  let query = req.body.query
  if(query[0] !== '{')
    query = '{' + query + '}'
  console.log(JSON.stringify({ query }))

  axios.post(BASE_URL, JSON.stringify({ query }), config)
    .then(response => res.json(response.data))
    .catch(error => next(error))
})

module.exports = router