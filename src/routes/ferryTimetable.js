const router = require('express').Router()
const {StopEnum, NextWeekdayTimestampEnum, getTimetableFor} = require('../util/digitransitHelper')

let cache
let cacheTime
router.get('/:stopId', (req, res, next) => {
  const weekdays = [
    NextWeekdayTimestampEnum.monday(),
    NextWeekdayTimestampEnum.tuesday(),
    NextWeekdayTimestampEnum.wednesday(),
    NextWeekdayTimestampEnum.friday(),
    NextWeekdayTimestampEnum.saturday(),
    NextWeekdayTimestampEnum.sunday()
  ]
  const stopId = req.params.stopId
  res.send(`/${stopId}`)
})