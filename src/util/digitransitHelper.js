const axios = require('axios')

const BASE_URL = 'http://api.digitransit.fi/routing/v1/routers/hsl/index/graphql'
const config = {
  headers: {
    'Content-Type': 'application/json',
  }
}

const nextWeekdayHelper = (weekday) => {
  const now = new Date()
  now.setHours(4, 0, 0, 0) // sets time 04:00am
  const nowWeekday = now.getDay() // 0 = sunday, 1 = monday, ..., 6 = saturday
  const weekdayDelta = Math.abs(weekday - nowWeekday)
  now.setDate(now.getDate() + weekdayDelta)
  return now.getTime()
}

const NextWeekdayTimestampEnum = Object.freeze({
  monday: () => nextWeekdayHelper(1),
  tuesday: () => nextWeekdayHelper(2),
  wednesday: () => nextWeekdayHelper(3),
  thursday: () => nextWeekdayHelper(4),
  friday: () => nextWeekdayHelper(5),
  saturday: () => nextWeekdayHelper(6),
  sunday: () => nextWeekdayHelper(0)
})

const StopEnum = Object.freeze({
  suomenlinna: 'HSL:1520702',
  kauppatori: 'HSL:1030701',
  katajanokka: 'HSL:1080701',
  huoltolaituri: 'HSL:1520703'
})

const getTimetableFor = async (stopId, startTime) => {
  const query = `{
    stop(id: "${stopId}") {
      name
      stoptimesWithoutPatterns(startTime: ${startTime / 1000}, timeRange: 86400, numberOfDepartures: 200) {
        scheduledDeparture
        serviceDay
        trip {
          tripHeadsign
        }
      }
    }
  }`

  const data = await axios.post(BASE_URL, JSON.stringify({ query }), config).then(res => res.data.data)
  const stopName = data.stop.name
  const stoptimes = data.stop.stoptimesWithoutPatterns
    .filter(item => item.trip.tripHeadsign !== stopName)
    .map(({ scheduledDeparture, serviceDay }) => ({ scheduledDeparture, serviceDay }))
    //.map(item => (item.scheduledDeparture + item.serviceDay)*1000 )

  return {stopName, stoptimes}
}

module.exports = {
  StopEnum,
  NextWeekdayTimestampEnum,
  getTimetableFor
}