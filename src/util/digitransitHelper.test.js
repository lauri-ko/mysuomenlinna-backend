const {getTimetableFor, NextWeekdayTimestampEnum, StopEnum} = require('./digitransitHelper')

describe('digitransitHelper', () => {
  describe('nextWeekdayTimestampEnum', () => {
    const weekdays = NextWeekdayTimestampEnum
    const stops = StopEnum
    it('values should get the timestamp for the next occurrence of the day', () => {
      let day = new Date(weekdays.monday())
      let weekday = day.getDay()
      expect(weekday).toBe(1)
      day = new Date(weekdays.tuesday())
      weekday = day.getDay()
      expect(weekday).toBe(2)
      day = new Date(weekdays.wednesday())
      weekday = day.getDay()
      expect(weekday).toBe(3)
      day = new Date(weekdays.thursday())
      weekday = day.getDay()
      expect(weekday).toBe(4)
      day = new Date(weekdays.friday())
      weekday = day.getDay()
      expect(weekday).toBe(5)
      day = new Date(weekdays.saturday())
      weekday = day.getDay()
      expect(weekday).toBe(6)
      day = new Date(weekdays.sunday())
      weekday = day.getDay()
      expect(weekday).toBe(0)
    })

  })

  describe('getTimetableFor', () => {
    it.only('should return departure times for the stop', async () => {
      const weekdays = NextWeekdayTimestampEnum
      const stops = StopEnum

      const stop = await getTimetableFor(stops.kauppatori, weekdays.monday())
      console.log(stop)
      expect(stop.stopName).toBe('Kauppatori')
      expect(stop.stoptimes).toBeDefined()
    })

  })
})